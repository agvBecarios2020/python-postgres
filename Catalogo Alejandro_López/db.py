import psycopg2

class Database():
    def __init__(self):
        pass
#Crear conexion DB
    def initalDataBase(self):
        try:
            connection=psycopg2.connect(# conexion db
                user= 'agavesoft',
                password = 'becarios2020',
                host='3.237.20.227',
                port='5432',
                database='Alejandro'
            )
            return connection
        except(Exception,psycopg2.Error) as error:#
            return ('Error de conexión',error)
    #Terminar conexion DB
    def DescoDatabase(self,connection):
        try:
            connection.close()
            return print('Desconectado')
        except(Exception,psycopg2.Error) as error:
            return ('Error de conexión',error)
    # nueva insercion en DB
    def create(self, connection, nombre, direccion,numerovehiculos,numeropersonas,productos):
            try:
                cursor = connection.cursor()# permite programar ProgrestSQL

                insertQuery = """ INSERT INTO sucursales2(NOMBRE,DIRECCION,NUMEROVEHICULOS,NUMEROPERSONAS,PRODUCTOS) VALUES (%s,%s,%s,%s,%s)"""
                recordInsert = (nombre, direccion,numerovehiculos,numeropersonas,productos)
                # Ejecuta el query, envio de datos
                cursor.execute(insertQuery, recordInsert)
                connection.commit()
                count = cursor.rowcount
                cursor.close()
                return count
            except (Exception, psycopg2.Error) as error :
                return ("Fallo insercion a la tabla", error)
    # Obtener todos los registros
    def select(self, connection):
            try:
                cursor = connection.cursor()
                selectQuery = """ SELECT * FROM sucursales2"""
                cursor.execute(selectQuery)
                sucursales = cursor.fetchall()
                connection.commit()

                cursor.close()
                return sucursales            
            except (Exception, psycopg2.Error) as error :
                return ("Fallo select a la tabla", error)

    # Actualizar registro 
    def update(self, connection, id, nombre, direccion,numerovehiculos,numeropersonas,productos):
        try:
            cursor = connection.cursor()
            if nombre != 'Null':
                # cambio de nombre 
                sql_update_query = """ UPDATE sucursales2 set nombre = %s where id = %s"""
                cursor.execute(sql_update_query, (nombre, id))
                connection.commit()
                count = cursor.rowcount
            if direccion != 'Null':
                # cambio de direccion
                sql_update_query = """ UPDATE sucursales2 set direccion = %s where id = %s"""
                cursor.execute(sql_update_query, (direccion, id))
                connection.commit()
                count = cursor.rowcount
            if numerovehiculos >= 0:
                # cambio de numero de vehiculos
                sql_update_query = """ UPDATE sucursales2 set numerovehiculos = %s where id = %s"""
                cursor.execute(sql_update_query, (numerovehiculos, id))
                connection.commit()
                count = cursor.rowcount
            if numeropersonas >= 0:
                # cambio de direccion
                sql_update_query = """ UPDATE sucursales2 set numeropersonas = %s where id = %s"""
                cursor.execute(sql_update_query, (numeropersonas, id))
                connection.commit()
                count = cursor.rowcount
            if productos != 'Null':
                # cambio de direccion
                sql_update_query = """ UPDATE sucursales2 set productos = %s where id = %s"""
                cursor.execute(sql_update_query, (productos, id))
                connection.commit()
                count = cursor.rowcount
            return (count, "Cambio realizado correctamente ")
        except (Exception, psycopg2.Error) as error:
            print("Error de Cambio ", error)
            
    # Eliminar registro
    def delete(self, connection, id):
        try:
            cursor = connection.cursor()
            
            # Eliminar una fila
            sql_delete_query = """DELETE FROM sucursales2 where id = %s"""
            cursor.execute(sql_delete_query, (id))
            count = cursor.rowcount
            connection.commit()
            return count

        except (Exception, psycopg2.Error) as error:
            return("Fallo eliminar fila ", error)

