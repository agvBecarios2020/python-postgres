import click

# Importar Clase
from db import Database

@click.group()
def crud():
    pass
#opciones de ingreso de datos por comando funcion create
@crud.command()
@click.option('--nombre', default='Null'
        , help='nombre de la sucursal')
@click.option('--direccion', default='Null'
        , help='direccion de la sucursal')
@click.option('--numerovehiculos', default=-1
        , help='Numero de Vehiculos')

@click.option('--numeropersonas', default=-1
        , help='Numero de personas en la sucursal')  
@click.option('--productos', default='Null'
        , help='Producto de la sucursal')   
def create( nombre, direccion,numerovehiculos,numeropersonas,productos):
    ''' Metodo Crear '''
    sucursal = dataBase.create(connection,nombre, direccion,numerovehiculos,numeropersonas,productos)
    if sucursal==1:
            click.echo("FILA CREADA:"+" "+ nombre + direccion+ str(numerovehiculos)+ str(numeropersonas) + productos)
    else:
            click.echo("FILA NO creada")

#opciones de ingreso de datos por comando funcion guardar
@crud.command()
@click.option('--id', default= -1
        , help='Id de la sucursal',)
@click.option('--nombre', default='Null'
        , help='nombre de la sucursal')
@click.option('--direccion', default='Null'
        , help='direccion de la sucursal')
@click.option('--numerovehiculos', default = -1
        , help='Numero de Vehiculos')
@click.option('--numeropersonas', default=-1
        , help='Numero de personas en la sucursal')  
@click.option('--productos', default='Null'
        , help='Producto de la sucursal') 
def update(id, nombre, direccion,numerovehiculos,numeropersonas,productos):
    ''' Metodo Update '''
    sucursal = dataBase.update(connection, id,nombre, direccion,numerovehiculos,numeropersonas,productos)
    click.echo(sucursal)

#opciones de ingreso de datos por comando funcion eliminar
@crud.command()
@click.option('--id', default=-1
        , help='Id de la sucursal',)
def delete(id):
    ''' Metodo Delete '''
    sucursal = dataBase.delete(connection, str(id))
    if sucursal==1:
            click.echo("FILA:"+" "+str(id)+" "+"ELIMINADA")
    else:
            click.echo("FILA NO ENCONTRADA")

#opciones de ingreso de datos por comando funcion mostrar  
@crud.command()
def select():
    ''' Metodo Select '''
    sucursal = dataBase.select(connection)
    for sucursal in sucursal:
        click.echo(sucursal)
if __name__ == "__main__":
    # Instancia de la base de datos
    dataBase = Database()

    # Inicar base de datos
    connection = dataBase.initalDataBase()

    # Grupo de comando crud
    crud()
